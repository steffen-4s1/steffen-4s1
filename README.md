# Welcome

My projects can be found at my [4s1 group](https://github.com/4s1-org) at GitHub.

More stuff can be found at
- [GitHub](https://github.com/steffen-4s1)
- [NPM](https://www.npmjs.com/~steffen-4s1)
- [SonarCloud](https://sonarcloud.io/organizations/4s1/projects)
